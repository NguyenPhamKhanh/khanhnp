import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import PassData from './passData';
import ReceiveData from './receiveData';

const Navigator = () => {
  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="PassData" component={PassData} />
        <Stack.Screen name="ReceiveData" component={ReceiveData} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
