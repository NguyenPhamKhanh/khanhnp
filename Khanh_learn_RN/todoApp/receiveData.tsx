/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable react-hooks/rules-of-hooks */
import React, {useCallback, useState} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';

const receiveData = ({route, navigation}) => {
  var itemData = route.params;
  const [name, setname] = useState(itemData.name);

  const goBack = () => {
    navigation.goBack();
  };

  const addButton = () => {
    itemData.onClick(name);
    navigation.goBack();
  };

  const editButton = () => {
    itemData.onClick(name);
    navigation.goBack();
  };

  const changeName = useCallback((name: string) => {
    setname(name);
  }, []);

  return (
    <View style={styles.passData}>
      <View style={styles.textInput}>
        <Text>{itemData.id}</Text>
        <TextInput
          style={styles.textInput}
          placeholder="fill name here"
          onChangeText={changeName}
          defaultValue={name}
        />
      </View>
      <View style={styles.button}>
        <Button title="OK" onPress={addButton} />
        <Button title="CanCel" onPress={goBack} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  passData: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  button: {
    flexDirection: 'row',
    marginTop: 30,
  },
  textInput: {
    alignItems: 'center',
    marginTop: 30,
    margin: 10,
    height: 100,
    padding: 15,
    borderRadius: 10,
  },
});

export default receiveData;
