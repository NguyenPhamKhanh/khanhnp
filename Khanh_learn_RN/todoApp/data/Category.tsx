class Category {
  id: number;
  name: string;
  live: boolean;
  constructor(id: number, name: string, live: boolean) {
    this.id = id;
    this.name = name;
    this.live = live;
  }
}

export default Category;
