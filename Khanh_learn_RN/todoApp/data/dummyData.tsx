import Category from './Category';
const CATEGORY = [
  new Category(1, 'Duong Ngoc Huyen', true),
  new Category(2, 'Bui Thi Chuyen', true),
  new Category(3, 'Le Viet Hoang', true),
  new Category(4, 'Tran Quoc Hoan', true),
  new Category(5, 'Cu Hoang Hai', false),
  new Category(6, 'Luong Thanh Tung', false),
  new Category(7, 'Trinh Qui Viet', false),
  new Category(8, 'Nguyen Phuong Trung', false),
  new Category(9, 'Nguyen Pham Khanh', false),
];

export default CATEGORY;
