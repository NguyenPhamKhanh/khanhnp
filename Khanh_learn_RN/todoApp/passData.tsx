/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/rules-of-hooks */
import React, {useState, useCallback} from 'react';
import {View, Text, StyleSheet, FlatList, Button} from 'react-native';
import CATEGORY from './data/dummyData';
import {TouchableOpacity, TextInput} from 'react-native-gesture-handler';

const passData = (props: any) => {
  const [isOnline, setisOnline] = useState(CATEGORY);
  const [isCoppyToSearch, setisCoppyToSearch] = useState(isOnline);
  const [test, setTest] = useState(0);

  const [addName, setAddName] = useState('');

  const handleOnChangeName = useCallback((name: string) => {
    setAddName(name);
  }, []);

  const handleSearch = useCallback(
    (name: string) => {
      setAddName(name);
      let obj = [];
      setisOnline(isCoppyToSearch);
      console.log('setisOnline isCoppyToSearch, ', isOnline.length);
      if (name !== '') {
        for (let i = 0; i < isOnline.length; i++) {
          let element = isOnline[i].name.toLowerCase();
          if (element.includes(name.toLowerCase())) {
            obj = obj.concat([
              {
                id: isOnline[i].id,
                name: isOnline[i].name,
                live: isOnline[i].live,
              },
            ]);
          }
        }
        setisOnline(obj);
        console.log('setisOnline obj, ', isOnline.length);
      }
    },
    [isCoppyToSearch, isOnline],
  );

  const findMaxID = () => {
    let element = 0;
    for (let i = 0; i < isOnline.length - 1; i++) {
      element = isOnline[i].id;
      if (isOnline[i].id < isOnline[i + 1].id) {
        element = isOnline[i + 1].id;
      }
    }
    return element + 1;
  };

  const AddItem = (name: string) => {
    let obj = [];
    obj = isOnline.concat([
      {
        id: findMaxID(),
        name: name,
        live: false,
      },
    ]);
    obj.sort(function (a, b) {
      return a.id - b.id;
    });
    setisCoppyToSearch(obj);
    setisOnline(obj);
    setAddName('');
  };

  const eventAdd = () => {
    props.navigation.navigate('ReceiveData', {
      id: findMaxID(),
      name: '',
      live: false,
      onClick: AddItem,
    });
  };

  const renderItem = (itemData: any) => {
    const editButton = (textInput) => {
      let obj = [];
      obj = isOnline
        .filter((edit) => edit.id !== itemData.item.id)
        .concat([
          {
            id: itemData.item.id,
            name: textInput,
            live: itemData.item.live,
          },
        ]);
      obj.sort(function (a, b) {
        return a.id - b.id;
      });
      setisOnline(obj);
    };

    const deleteItem = () => {
      let obj = [];
      obj = isOnline.filter((edit) => edit.id !== itemData.item.id);
      obj.sort(function (a, b) {
        return a.id - b.id;
      });
      setisOnline(obj);
      setisCoppyToSearch(obj);
    };

    return (
      <TouchableOpacity
        style={itemData.item.live ? styles.online : styles.offline}
        onPress={deleteItem}>
        <TouchableOpacity onPress={deleteItem}>
          <Text>{itemData.item.name}</Text>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  return (
    <View>
      <View>
        <TextInput
          style={styles.text}
          placeholder="fill name here"
          defaultValue={addName}
          onChangeText={handleSearch}
        />
        <View>
          <Button title="ADD" onPress={eventAdd} />
        </View>
      </View>
      <FlatList
        data={isOnline}
        keyExtractor={(item, index) => item.name}
        renderItem={renderItem}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  online: {
    backgroundColor: '#fd79a8',
    marginTop: 30,
    margin: 10,
    height: 50,
    padding: 15,
    borderRadius: 10,
  },
  offline: {
    backgroundColor: 'grey',
    marginTop: 30,
    margin: 10,
    height: 50,
    padding: 15,
    borderRadius: 10,
  },
  View: {
    textAlign: 'center',
    flexDirection: 'row',
  },
  text: {
    marginTop: 30,
    margin: 10,
    height: 50,
    padding: 15,
  },
  button: {
    marginTop: 30,
    margin: 10,
    height: 50,
    padding: 15,
  },
});

export default passData;
