import {INCREMENT, DECREMENT} from '../redux/NumberApp/action';
//saga effect
import {takeEvery} from 'redux-saga/effects';

export function* sayHello() {
  console.log('sayHello');
}

function* increment() {
  console.log('this is increment');
}

export function* watchIncrement() {
  yield takeEvery(INCREMENT, increment);
}

function* decrement() {
  console.log('this is decrement');
}

export function* watchDecrement() {
  yield takeEvery(DECREMENT, decrement);
}
