import {all} from 'redux-saga/effects'; //chay tat ca saga 1 luc

import {sayHello, watchDecrement, watchIncrement} from './counterSaga';

export default function* rootSaga() {
  yield all([sayHello, watchDecrement(), watchIncrement()]);
}
