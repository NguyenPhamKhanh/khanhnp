// /* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
//App number
// import {Provider} from 'react-redux';
// import {createStore} from 'redux';
// import NumberApp from './redux/NumberApp/NumberApp';
// import createSagaMiddleware from 'redux-saga';
// import Reducer from './redux/NumberApp/NumberApp';
// import rootSaga from './reduxSaga/rootSaga';

//ShoppingApp
// import Navigator from './ShoppingApp/navigator/Navigator';
// import store from './ShoppingApp/redux/store';
// import {Provider} from 'react-redux';

// //app Login
import {Provider} from 'react-redux';
import Navigator from './demo_login/Navigator/Navigator';
import store from './demo_login/redux/store/index';

//app todo
// // import Navigator from './todoApp/Navigator';
// import AllReducers from './redux/NumberApp/index.reducer';
// import NumberApp from './redux/NumberApp/NumberApp';
// import createSagaMiddleware from 'redux-saga';
// import rootSaga from './reduxSaga/rootSaga';

// app NumberApp
// const sagaMiddleware = createSagaMiddleware();
// let store = createStore(Reducer);
// const App = () => {
//   return (
//     <Provider store={store}>
//       <NumberApp />
//     </Provider>
//   );
// };
// sagaMiddleware.run(rootSaga); //rootSaga
// export default App;

const App = () => {
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
    // <Test />
  );
};
export default App;
