export interface IFriend {
  id: string;
  name: string;
  hometown: string;
}
