import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook,
  useDispatch,
} from 'react-redux';
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;

import {RootState} from '../redux/reducer';
import LoginScreen from '../screen/Login/LoginScreen';
import HomeScreen from '../screen/Home/HomeScreen';
import addScreen from '../screen/addScreen/addScreen';
import {CHECK_AUTH} from '../redux/action/homeAction';

const Navigator = () => {
  const dipatch = useDispatch();
  // const isAuthentication = useSelector(
  //   (state: RootState) => state.homeReducer.isAuthentication,
  // );
  const token = useSelector((state: RootState) => state.homeReducer.token);

  useEffect(() => {
    dipatch(CHECK_AUTH());
  }, [dipatch]);

  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {token === null || token === undefined ? (
          <Stack.Screen
            name="LoginScreen"
            component={LoginScreen}
            options={{headerShown: false}}
          />
        ) : (
          <Stack.Screen
            name="HomeScreen"
            component={HomeScreen}
            options={{headerShown: false}}
          />
        )}
        <Stack.Screen
          name="addScreen"
          component={addScreen}
          options={{headerShown: true}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
