/* eslint-disable no-alert */
const InitialState = {
  username: '',
  password: '',
  token: '',
};

const loginReducer = (state = InitialState, action) => {
  if (action.type === 'LOGIN_REQUEST') {
    return {
      ...state,
    };
  }

  if (action.type === 'LOGIN_FAIL') {
    alert('Email or password is wrong');
    return {
      ...state,
    };
  }
  return state;
};

export default loginReducer;
