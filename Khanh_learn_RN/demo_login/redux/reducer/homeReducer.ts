import {products} from '../../mockup/friends';

const InitialState = {
  isAuthentication: false,
  token: '',
  arrFriend: products,
};

const loginReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCES':
      return {
        ...state,
        isAuthentication: true,
        token: action.token,
      };
    case 'LOGOUT_SUCCES':
      return {
        ...state,
        isAuthentication: false,
        token: null,
      };
    case 'CHECK_AUTH_SUCCESS':
      return {
        ...state,
        isAuthentication: !!action.payload,
        token: action.payload,
      };
    case 'DELETE_SUCCESS':
      return {
        ...state,
        arrFriend: action.payload,
      };
    case 'ADD_SUCCESS':
      return {
        ...state,
        arrFriend: action.payload,
      };
    case 'EDIT_SUCCESS':
      return {
        ...state,
        arrFriend: action.payload,
      };
  }

  return state;
};

export default loginReducer;
