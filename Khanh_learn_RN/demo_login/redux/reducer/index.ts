import {combineReducers} from 'redux';
import loginReducer from './loginReducer';
import homeReducer from './homeReducer';

export const rootReducer = combineReducers({
  loginReducer,
  homeReducer,
});
export type RootState = ReturnType<typeof rootReducer>;
