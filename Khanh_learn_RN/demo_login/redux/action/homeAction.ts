import * as types from '../constants';

export const CHECK_AUTH = () => {
  return {type: types.CHECK_AUTH};
};

export const LOGOUT_REQUEST = () => {
  return {
    type: types.LOGOUT_REQUEST,
  };
};

export const LOGOUT_SUCCES = () => {
  return {
    type: types.LOGOUT_SUCCES,
  };
};

export const DELETE_REQUEST = (id: string, arrFriend: object) => {
  return {
    type: types.DELETE_REQUEST,
    id: id,
    arrFriend: arrFriend,
  };
};

export const DELETE_SUCCESS = () => {
  return {
    type: types.DELETE_SUCCESS,
  };
};

export const ADD_REQUEST = (id: number, name: string, hometown: string) => {
  return {
    type: types.ADD_REQUEST,
    id: id,
    name: name,
    hometown: hometown,
  };
};

export const ADD_SUCCESS = () => {
  return {
    type: types.ADD_SUCCESS,
  };
};

export const EDIT_REQUEST = (id: number, name: string, hometown: string) => {
  return {
    type: types.EDIT_REQUEST,
    id: id,
    name: name,
    hometown: hometown,
  };
};

export const EDIT_SUCCESS = () => {
  return {
    type: types.EDIT_SUCCESS,
  };
};
