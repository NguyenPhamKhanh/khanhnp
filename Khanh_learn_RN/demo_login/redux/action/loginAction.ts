import * as types from '../constants';

export const LOGIN_REQUEST = (username: string, password: string) => {
  return {
    type: types.LOGIN_REQUEST,
    username: username,
    password: password,
  };
};

export const LOGIN_SUCCES = (token: string) => {
  return {
    type: types.LOGIN_SUCCES,
    token: token,
  };
};

export const LOGIN_FAIL = () => {
  return {
    type: types.LOGIN_FAIL,
  };
};
