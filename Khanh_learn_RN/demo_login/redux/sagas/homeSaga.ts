import {takeLatest, put, call, select} from 'redux-saga/effects';
import AsyncStorage from '@react-native-community/async-storage';

import {LOGOUT_SUCCES} from '../action/homeAction';
import * as types from '../constants';

function* checkAuthRequest() {
  try {
    const token = yield call(AsyncStorage.getItem, 'tokenLogin');
    yield put({type: types.CHECK_AUTH_SUCCESS, payload: token || null});
  } catch (e) {
    yield put({type: types.CHECK_AUTH_FAILED});
  }
}

const _receiveLogOut = async () => {
  try {
    await AsyncStorage.removeItem('tokenLogin');
  } catch (error) {
    // Error retrieving data
  }
};

function* logoutRequest() {
  _receiveLogOut();
  yield put(LOGOUT_SUCCES());
}

//delete
function* deleteRequest(data) {
  data.arrFriend = data.arrFriend.filter((state) => state.id !== data.id);
  yield put({type: types.DELETE_SUCCESS, payload: data.arrFriend});
}

//ADD
function* addRequest(data) {
  const arrFriend = yield select((store) => store.homeReducer.arrFriend);
  const payload = arrFriend.concat([
    {
      id: data.id,
      name: data.name,
      hometown: data.hometown,
    },
  ]);

  yield put({type: types.ADD_SUCCESS, payload: payload});
}

//edit
function* editRequest(data) {
  const arrFriend = yield select((store) => store.homeReducer.arrFriend);
  const payload = arrFriend.map((item) => {
    if (item.id === data.id) {
      return {
        id: data.id,
        name: data.name,
        hometown: data.hometown,
      };
    }
    return item;
  });

  yield put({type: types.EDIT_SUCCESS, payload});
}

export function* logoutRequestSaga() {
  yield takeLatest(types.LOGOUT_REQUEST, logoutRequest);
}

export function* checkAuthRequestSaga() {
  yield takeLatest(types.CHECK_AUTH, checkAuthRequest);
}

export function* deleteRequestSaga() {
  yield takeLatest(types.DELETE_REQUEST, deleteRequest);
}

export function* addRequestSaga() {
  yield takeLatest(types.ADD_REQUEST, addRequest);
}

export function* editRequestSaga() {
  yield takeLatest(types.EDIT_REQUEST, editRequest);
}
