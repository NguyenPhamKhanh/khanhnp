import {takeLatest, call, put} from 'redux-saga/effects';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import {LOGIN_SUCCES, LOGIN_FAIL} from '../action/loginAction';
import * as types from '../constants';

export function* loginRequest(action) {
  const getUrl =
    'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBW2Bdms_L9lZpdQJa3RQpSpZoGwhxyeIk';
  try {
    const data = yield call(axios.request, {
      url: getUrl,
      method: 'POST',
      data: {
        email: action.username, // anhduc.hoang@vmodev.com
        password: action.password, //12345678
      },
    });
    if (data) {
      AsyncStorage.setItem('tokenLogin', data.data.idToken);
      yield put(LOGIN_SUCCES(data.data.idToken));
    }
  } catch (error) {
    yield put(LOGIN_FAIL());
  }
}

export function* loginRequestSaga() {
  yield takeLatest(types.LOGIN_REQUEST, loginRequest);
}
