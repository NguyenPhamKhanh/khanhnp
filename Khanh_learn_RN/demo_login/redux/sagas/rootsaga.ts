import {all} from 'redux-saga/effects';

import {loginRequestSaga} from './loginSaga';
import {
  logoutRequestSaga,
  deleteRequestSaga,
  checkAuthRequestSaga,
  addRequestSaga,
  editRequestSaga,
} from './homeSaga';

export default function* rootSaga() {
  yield all([
    loginRequestSaga(),
    logoutRequestSaga(),
    checkAuthRequestSaga(),
    deleteRequestSaga(),
    addRequestSaga(),
    editRequestSaga(),
  ]);
}
