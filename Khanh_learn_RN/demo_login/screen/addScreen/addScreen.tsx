/* eslint-disable radix */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/rules-of-hooks */
import React, {useCallback, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook,
  useDispatch,
} from 'react-redux';
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;

import {RootState} from '../../redux/reducer';
import {ADD_REQUEST, EDIT_REQUEST} from '../../redux/action/homeAction';

const findID = (
  arrFriend: Array<{id: string; name: string; hometown: string}>,
) => {
  var element = '';
  if (arrFriend.length === 0) {
    return 1;
  } else if (arrFriend.length === 1) {
    return parseInt(arrFriend[0].id) + 1;
  } else {
    for (let i = 0; i < arrFriend.length - 1; i++) {
      element = arrFriend[i].id;
      if (element < arrFriend[i + 1].id) {
        element = arrFriend[i + 1].id;
      }
    }
  }
  return parseInt(element) + 1;
};

const addScreen = ({route, navigation}) => {
  const dispatch = useDispatch();
  const arrFriend = useSelector(
    (state: RootState) => state.homeReducer.arrFriend,
  );

  const receiveData = route.params;
  var receiveID = findID(arrFriend);
  var receiveName = '';
  var receiveHometown = '';

  //edit
  if (receiveData.id !== 0) {
    receiveID = receiveData.id;
    receiveName = receiveData.name;
    receiveHometown = receiveData.hometown;
  }

  const [id, setID] = useState(receiveID);
  const [name, setName] = useState(receiveName);
  const [homeTown, setHomeTown] = useState(receiveHometown);

  const handleChangeName = useCallback(
    (data: string) => {
      setName(data);
    },
    [setName],
  );

  const handleChangeHomeTown = useCallback(
    (data: string) => {
      setHomeTown(data);
    },
    [setHomeTown],
  );

  const handlerOnPressOk = () => {
    //edit
    if (receiveData.id !== 0) {
      dispatch(EDIT_REQUEST(id, name, homeTown));
    } else {
      //add
      dispatch(ADD_REQUEST(id, name, homeTown));
    }
    navigation.goBack();
  };

  const handlerOnPressCancel = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.view}>
      <View>
        <Text style={styles.textID}>{id}</Text>
        <TextInput
          value={name}
          placeholder="name"
          onChangeText={handleChangeName}
          style={styles.textInputName}
        />
        <TextInput
          value={homeTown}
          placeholder="HomeTown"
          onChangeText={handleChangeHomeTown}
          style={styles.textInputHomeTown}
        />
      </View>
      <View style={styles.viewTouchable}>
        <TouchableOpacity onPress={handlerOnPressOk}>
          <Text style={styles.textOkCancel}> Ok </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={handlerOnPressCancel}>
          <Text style={styles.textOkCancel}> Cancel </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewTouchable: {
    flexDirection: 'row',
    marginTop: 30,
  },
  textID: {
    width: 200,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
  textInputName: {
    width: 200,
    height: 40,
    marginTop: 30,
    textAlign: 'center',
  },
  textInputHomeTown: {
    width: 200,
    height: 40,
    marginTop: 30,
    textAlign: 'center',
  },
  textOkCancel: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    marginLeft: 20,
  },
});

export default addScreen;
