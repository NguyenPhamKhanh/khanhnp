import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  view0: {
    flex: 1,
  },
  view1: {
    height: 300,
  },
  text1: {
    fontSize: 30,
    marginTop: 250,
    textAlign: 'center',
  },
  view2: {
    height: 300,
  },
  view31: {
    // backgroundColor: 'blue',
    height: 50,
    flexDirection: 'row',
  },
  text311: {
    fontSize: 20,
    marginTop: 10,
    marginLeft: 40,
  },
  text312: {
    fontSize: 20,
    marginLeft: 10,
  },
  view32: {
    // backgroundColor: 'green',
    marginTop: 15,
    height: 50,
    flexDirection: 'row',
  },
  text321: {
    fontSize: 20,
    marginTop: 10,
    marginLeft: 40,
  },
  text322: {
    fontSize: 20,
    marginLeft: 10,
  },
  touchable: {
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  textTouch: {
    fontSize: 20,
    color: 'red',
  },
});

export default styles;
