import React, {useCallback, useState} from 'react';
import {View, Text} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch} from 'react-redux';

import styles from './styleLogin';
import {LOGIN_REQUEST} from '../../redux/action/loginAction';

const LoginScreen = () => {
  const [userName, setUserName] = useState('');
  const [passWord, setPassWord] = useState('');

  const dispatch = useDispatch();

  const handleOnChangeUserName = useCallback((data: string) => {
    setUserName(data);
  }, []);

  const handleOnChangePassWord = useCallback((data: string) => {
    setPassWord(data);
  }, []);

  const handleOnPressLogin = () => {
    dispatch(LOGIN_REQUEST(userName, passWord));
    //Expressions
    //dom
    //opacityable - feedback
    //useEffect class
    //login -> todo //drag 1 2 3 => 1 3 2
    //rule of hook
    // kieu du lieu:
    // hoi-ting -> kiểu dữ liệu dc declare ở đầu hàm
    // so sanh == và ===
    // useCallback su dung khi 1 function dang chay nhung ta can doi no chay xong
    // Closures
    //
    //
  };

  return (
    <View style={styles.view0}>
      <View style={styles.view1}>
        <Text style={styles.text1}>Login</Text>
      </View>
      <View style={styles.view2}>
        <View style={styles.view31}>
          <Text style={styles.text311}>username:</Text>
          <TextInput
            style={styles.text312}
            placeholder="fill username"
            onChangeText={handleOnChangeUserName}
          />
        </View>
        <View style={styles.view32}>
          <Text style={styles.text321}>password:</Text>
          <TextInput
            style={styles.text322}
            secureTextEntry={true}
            placeholder="fill password"
            onChangeText={handleOnChangePassWord}
          />
        </View>
        <TouchableOpacity style={styles.touchable} onPress={handleOnPressLogin}>
          <Text style={styles.textTouch}>Đăng nhập</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginScreen;
