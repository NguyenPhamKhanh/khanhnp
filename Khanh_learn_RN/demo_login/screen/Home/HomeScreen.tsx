/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useState, useEffect, useCallback} from 'react';
import {Text, View, ImageBackground} from 'react-native';
import {TouchableOpacity, TextInput} from 'react-native-gesture-handler';
import DraggableFlatList from 'react-native-draggable-flatlist';
import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook,
  useDispatch,
} from 'react-redux';
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;

import {RootState} from '../../redux/reducer';
import {LOGOUT_REQUEST, DELETE_REQUEST} from '../../redux/action/homeAction';
import styles from './styleHome';

const HomeScreen = (props) => {
  const dispatch = useDispatch();
  const arrFriend = useSelector(
    (state: RootState) => state.homeReducer.arrFriend,
  );

  const [state, setstate] = useState(arrFriend);
  const [isArrToSearch, setisArrToSearch] = useState(arrFriend);

  const handlerLogOut = () => {
    dispatch(LOGOUT_REQUEST());
  };

  const handlerAdd = () => {
    props.navigation.navigate('addScreen', {
      id: 0,
    });
  };

  const handlerOnSearch = useCallback(
    (name: string) => {
      if (name.trim() !== '') {
        setstate(
          isArrToSearch.filter((item) =>
            item.name.toUpperCase().includes(name.toUpperCase().trim()),
          ),
        );
      } else {
        setstate(isArrToSearch);
      }
    },
    [isArrToSearch],
  );

  //khi arrFriend change => homeScreen not be rendered
  useEffect(() => {
    setstate(arrFriend);
  }, [arrFriend]);

  const renderItem = ({item, index, drag, isActive}) => {
    const handlerDelete = () => {
      dispatch(DELETE_REQUEST(item.id, state));
    };

    const handlerEdit = () => {
      props.navigation.navigate('addScreen', {
        id: item.id,
        name: item.name,
        hometown: item.hometown,
      });
    };

    return (
      <View style={styles.viewCha}>
        <TouchableOpacity onLongPress={drag} style={styles.flatList}>
          <View style={styles.viewImage}>
            <Text style={styles.textFlatlist}>{item.name}</Text>
            <TouchableOpacity
              style={styles.touchableEdit}
              onPress={handlerEdit}>
              <ImageBackground
                source={{
                  uri:
                    'https://cdn0.iconfinder.com/data/icons/glyphpack/19/edit-512.png',
                }}
                style={styles.imageEdit}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.touchableDelete}
              onPress={handlerDelete}>
              <ImageBackground
                source={{
                  uri:
                    'https://cdn.iconscout.com/icon/premium/png-512-thumb/delete-1432400-1211078.png',
                }}
                style={styles.imageDelte}
              />
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>
    );
  };
  //
  return (
    <View style={styles.leader}>
      <View style={styles.header}>
        <TouchableOpacity onPress={handlerLogOut} style={styles.touchForLogOut}>
          <ImageBackground
            source={{
              uri:
                'https://cdn.iconscout.com/icon/free/png-512/power-off-1646691-1400192.png',
            }}
            style={styles.image}
          />
        </TouchableOpacity>
        <View style={styles.viewAddSearch}>
          <TextInput
            style={styles.textInputSearch}
            placeholder="search friend "
            onChangeText={handlerOnSearch}
          />
          <TouchableOpacity style={styles.touchAdd} onPress={handlerAdd}>
            <ImageBackground
              source={{
                uri:
                  'https://cdn3.iconfinder.com/data/icons/basic-ui-elementsblue-1/389467/36-2-512.png',
              }} //add
              style={styles.image}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.mid}>
        <DraggableFlatList
          data={state}
          keyExtractor={(item) => `draggable-item-${item.id}`}
          renderItem={renderItem}
          onDragEnd={({data}) => setstate(data)}
        />
      </View>
      <View style={styles.footer}>
        <Text> </Text>
      </View>
    </View>
  );
};

export default HomeScreen;
