import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  viewCha: {
    flex: 1,
  },
  leader: {
    flex: 1,
  },
  header: {
    flex: 3,
    justifyContent: 'center',
  },
  image: {
    width: 30,
    height: 30,
  },
  touchForLogOut: {
    marginTop: 15,
    marginLeft: 330,
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    height: 50,
  },
  viewAddSearch: {
    marginLeft: 20,
    marginTop: 20,
    width: 250,
    flexDirection: 'row',
  },
  touchAdd: {
    width: 30,
    height: 30,
    marginLeft: 20,
  },
  textInputSearch: {
    width: 300,
    height: 30,
    fontWeight: 'bold',
    borderWidth: 1,
  },
  viewImage: {
    flexDirection: 'row',
    height: 70,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  touchableDelete: {
    marginRight: 20,
  },
  touchableEdit: {
    marginLeft: 40,
    marginRight: 10,
  },
  imageDelte: {
    width: 40,
    height: 40,
  },
  imageEdit: {
    width: 30,
    height: 30,
  },
  mid: {
    flex: 13,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  flatList: {
    height: 70,
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: 'green',
    borderRadius: 40,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  textFlatlist: {
    marginLeft: 30,
    fontSize: 20,
    fontWeight: 'bold',
  },
  footer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'blue',
  },
});

export default styles;
