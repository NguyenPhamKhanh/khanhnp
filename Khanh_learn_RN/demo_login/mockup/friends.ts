import {IFriend} from '../models/Friend';

export const products: IFriend[] = [
  {
    id: '1',
    name: 'Duong Ngoc Huyen',
    hometown: 'Thai Binh',
  },
  {
    id: '2',
    name: 'Bui Thi Chuyen',
    hometown: 'Thai Binh',
  },
  {
    id: '3',
    name: 'Cu Hoang Hai',
    hometown: 'Ha Noi',
  },
  {
    id: '4',
    name: 'Luong Thanh Tung',
    hometown: 'Ha Noi',
  },
  {
    id: '5',
    name: 'Tran Quoc Hoan',
    hometown: 'Ha Noi',
  },
  {
    id: '6',
    name: 'Trinh Qui Viet',
    hometown: 'Ha Noi',
  },
  {
    id: '7',
    name: 'Nguyen Phuong Trung',
    hometown: 'Ha Noi',
  },
  {
    id: '8',
    name: 'Le Viet Hoang',
    hometown: 'Thai Binh',
  },
  {
    id: '9',
    name: 'Nguyen Pham KHanh',
    hometown: 'Ha Noi',
  },
];
