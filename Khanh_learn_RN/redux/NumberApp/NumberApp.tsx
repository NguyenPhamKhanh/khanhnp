import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';

import {INCREMENT, DECREMENT} from './constants';

const NumberApp = () => {
  const dispatch = useDispatch();
  const counter = useSelector((state: any) => state.Reducer.value);

  const increment = () => {
    dispatch(INCREMENT());
  };

  const decrement = () => {
    dispatch(DECREMENT());
  };

  return (
    <View style={styles.view}>
      <TouchableOpacity onPress={increment} style={styles.components}>
        <Text style={styles.text}>+</Text>
      </TouchableOpacity>
      <Text style={styles.text}>{counter}</Text>
      <TouchableOpacity onPress={decrement} style={styles.components}>
        <Text style={styles.text}>-</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  components: {
    padding: 30,
  },
  text: {
    fontSize: 40,
  },
});

export default NumberApp;
