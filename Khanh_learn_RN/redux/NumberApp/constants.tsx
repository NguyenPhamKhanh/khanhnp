import * as types from './action';

export const INCREMENT = () => {
  return {
    type: types.INCREMENT,
  };
};

export const DECREMENT = () => {
  return {
    type: types.DECREMENT,
  };
};
