// import React from 'react';

const defaultState = {value: 0};

const Reducer = (state = defaultState, action) => {
  if (action.type === 'INCREMENT') {
    return {value: state.value + 1};
  }
  if (action.type === 'DECREMENT') {
    return {value: state.value - 1};
  }
  return state;
};

export default Reducer;
