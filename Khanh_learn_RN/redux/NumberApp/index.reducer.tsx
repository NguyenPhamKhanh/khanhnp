import {combineReducers} from 'redux';
import Reducer from './reducer';

const AllReducers = combineReducers({
  Reducer,
});

export default AllReducers;
