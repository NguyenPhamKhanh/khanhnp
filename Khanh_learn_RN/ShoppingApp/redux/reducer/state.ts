import {IProduct} from '../../models/Product';

export interface ICartState {
  products: Map<string, number>;
}

export interface IProductState {
  products: IProduct[];
}
