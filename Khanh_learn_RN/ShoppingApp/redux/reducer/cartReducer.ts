/* eslint-disable @typescript-eslint/no-unused-vars */
import {ICartState} from './state';
import {CartData} from '../../mockup/products';

const initialState = {
  //:ICartState = {
  // products: null,
  cart: CartData,
};

const cartReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case 'INCREMENTCART': {
      return state;
    }
    case 'INCREMENTCARTSUCCES': {
      return {
        ...state,
        cart: action.newCartData,
      };
    }
    case 'DECREMENTCART': {
      return state;
    }
    case 'DECREMENTCARTSUCCES': {
      return {
        ...state,
        cart: action.newCartData,
      };
    }
    case 'EMPTYCART': {
      return {
        cart: [],
      };
    }
    default:
      return state;
  }
};

export default cartReducer;
