// import {IProductState} from './state';
import {products} from '../../mockup/products';

// const initialState: IProductState = {
//   products: mockupProducts,
// };

const initialState = {
  product: products,
};

const productReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case 'UPDATEPRODUCTAMOUNT':
      return state;
    case 'UPDATEPRODUCTAMOUNTSUCCES':
      return {
        ...state,
        product: action.productData,
      };
    default:
      return state;
  }
};

export default productReducer;
