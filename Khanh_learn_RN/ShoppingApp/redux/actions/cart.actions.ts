import * as types from '../constants/index';

export const IncrementCart = (
  data: object,
  productData: object,
  cartData: object,
) => {
  return {
    type: types.INCREMENTCART,
    data: data,
    productData: productData,
    cartData: cartData,
  };
};

export const IncrementCartSucces = (newCartData: object) => {
  return {
    type: types.INCREMENTCARTSUCCES,
    newCartData: newCartData,
  };
};

export const DecrementCart = (
  id: string,
  productData: object,
  cartData: object,
) => {
  return {
    type: types.DECREMENTCART,
    id: id,
    productData: productData,
    cartData: cartData,
  };
};

export const DecrementCartSucces = (newCartData: object) => {
  return {
    type: types.DECREMENTCARTSUCCES,
    newCartData: newCartData,
  };
};

export const emptyCart = () => {
  return {
    type: types.EMPTYCART,
  };
};
