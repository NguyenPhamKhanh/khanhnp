import * as types from '../constants/index';

export const updateProductAmount = (productData: object, cartData: object) => {
  return {
    type: types.UPDATEPRODUCTAMOUNT,
    productData: productData,
    cartData: cartData,
  };
};

export const updateProductAmountSucces = (productData: object) => {
  return {
    type: types.UPDATEPRODUCTAMOUNTSUCCES,
    productData: productData,
  };
};
