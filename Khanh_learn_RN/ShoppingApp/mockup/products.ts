import {IProduct} from '../models/Product';

export const products: IProduct[] = [
  {
    id: 'MH370',
    name: 'iphone 12 pro max',
    url1:
      'https://images.dailyhive.com/20201022050311/Screen-Shot-2020-10-22-at-8.02.42-AM.png',
    capacity: '512gb',
    price: 37990000,
    amount: 5,
    screenSize: '6.7", Lớp phủ oleophobic chống bám vân tay',
    screenResolution: '2778 x 1284 pixel 458 ppi',
    cpu: 'A14 Bionci',
    ram: '6 GB',
    url2:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Graphite.png?v=1603890438',
    url3:
      'https://bizweb.dktcdn.net/100/116/615/products/12promax.png?v=1602751668000',
    url4:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Gold_1200x1200.png?v=1603890438',
    url5:
      'https://static.orangeromania.ro/webshop-images/hds_app0102602/apple_apple_iphone_12_pro_max_512gb_silver_l59358_14.png',
    mainCamera:
      'Siêu rộng: khẩu độ ƒ / 2.4. Nắp ống kính tinh thể sapphire, 100% Focus Pixels (Rộng), Hệ thống camera 12MP chuyên nghiệp',
    pin: '3687 mhA',
  },
  {
    id: 'MH371',
    name: 'iphone 12 pro max',
    url1:
      'https://images.dailyhive.com/20201022050311/Screen-Shot-2020-10-22-at-8.02.42-AM.png',
    capacity: '256gb',
    price: 32990000,
    amount: 3,
    screenSize: '6.7", Lớp phủ oleophobic chống bám vân tay',
    screenResolution: '2778 x 1284 pixel 458 ppi',
    cpu: 'A14 Bionci',
    ram: '6 GB',
    url2:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Graphite.png?v=1603890438',
    url3:
      'https://bizweb.dktcdn.net/100/116/615/products/12promax.png?v=1602751668000',
    url4:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Gold_1200x1200.png?v=1603890438',
    url5:
      'https://static.orangeromania.ro/webshop-images/hds_app0102602/apple_apple_iphone_12_pro_max_512gb_silver_l59358_14.png',
    mainCamera:
      'Siêu rộng: khẩu độ ƒ / 2.4. Nắp ống kính tinh thể sapphire, 100% Focus Pixels (Rộng), Hệ thống camera 12MP chuyên nghiệp',
    pin: '3687 mhA',
  },
  {
    id: 'MH372',
    name: 'iphone 12 pro max',
    url1:
      'https://images.dailyhive.com/20201022050311/Screen-Shot-2020-10-22-at-8.02.42-AM.png',
    capacity: '128gb',
    price: 29990000,
    amount: 5,
    screenSize: '6.7", Lớp phủ oleophobic chống bám vân tay',
    screenResolution: '2778 x 1284 pixel 458 ppi',
    cpu: 'A14 Bionci',
    ram: '6 GB',
    url2:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Graphite.png?v=1603890438',
    url3:
      'https://bizweb.dktcdn.net/100/116/615/products/12promax.png?v=1602751668000',
    url4:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Gold_1200x1200.png?v=1603890438',
    url5:
      'https://static.orangeromania.ro/webshop-images/hds_app0102602/apple_apple_iphone_12_pro_max_512gb_silver_l59358_14.png',
    mainCamera:
      'Siêu rộng: khẩu độ ƒ / 2.4. Nắp ống kính tinh thể sapphire, 100% Focus Pixels (Rộng), Hệ thống camera 12MP chuyên nghiệp',
    pin: '3687 mhA',
  },
  {
    id: 'MH373',
    name: 'iphone 12 pro',
    url1:
      'https://images.dailyhive.com/20201022050311/Screen-Shot-2020-10-22-at-8.02.42-AM.png',
    capacity: '512gb',
    price: 35490000,
    amount: 5,
    screenSize: '6.7", Lớp phủ oleophobic chống bám vân tay',
    screenResolution: '2532 x 1170 pixel 460 ppi',
    cpu: 'A14 Bionci',
    ram: '6 GB',
    url2:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Graphite.png?v=1603890438',
    url3:
      'https://bizweb.dktcdn.net/100/116/615/products/12promax.png?v=1602751668000',
    url4:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Gold_1200x1200.png?v=1603890438',
    url5:
      'https://static.orangeromania.ro/webshop-images/hds_app0102602/apple_apple_iphone_12_pro_max_512gb_silver_l59358_14.png',
    mainCamera:
      'Siêu rộng: khẩu độ ƒ / 2.4. Nắp ống kính tinh thể sapphire, 100% Focus Pixels (Rộng), Hệ thống camera 12MP chuyên nghiệp',
    pin: '2815 mhA',
  },
  {
    id: 'MH374',
    name: 'iphone 12 pro',
    url1:
      'https://images.dailyhive.com/20201022050311/Screen-Shot-2020-10-22-at-8.02.42-AM.png',
    capacity: '256gb',
    price: 30290000,
    amount: 5,
    screenSize: '6.7", Lớp phủ oleophobic chống bám vân tay',
    screenResolution: '2532 x 1170 pixel 460 ppi',
    cpu: 'A14 Bionci',
    ram: '6 GB',
    url2:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Graphite.png?v=1603890438',
    url3:
      'https://bizweb.dktcdn.net/100/116/615/products/12promax.png?v=1602751668000',
    url4:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Gold_1200x1200.png?v=1603890438',
    url5:
      'https://static.orangeromania.ro/webshop-images/hds_app0102602/apple_apple_iphone_12_pro_max_512gb_silver_l59358_14.png',
    mainCamera:
      'Siêu rộng: khẩu độ ƒ / 2.4. Nắp ống kính tinh thể sapphire, 100% Focus Pixels (Rộng), Hệ thống camera 12MP chuyên nghiệp',
    pin: '2815 mhA',
  },
  {
    id: 'MH375',
    name: 'iphone 12 pro',
    url1:
      'https://images.dailyhive.com/20201022050311/Screen-Shot-2020-10-22-at-8.02.42-AM.png',
    capacity: '128gb',
    price: 27450000,
    amount: 5,
    screenSize: '6.7", Lớp phủ oleophobic chống bám vân tay',
    screenResolution: '2532 x 1170 pixel 460 ppi',
    cpu: 'A14 Bionci',
    ram: '6 GB',
    url2:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Graphite.png?v=1603890438',
    url3:
      'https://bizweb.dktcdn.net/100/116/615/products/12promax.png?v=1602751668000',
    url4:
      'https://cdn.shopify.com/s/files/1/1684/4603/products/iphone-12-pro-max_Gold_1200x1200.png?v=1603890438',
    url5:
      'https://static.orangeromania.ro/webshop-images/hds_app0102602/apple_apple_iphone_12_pro_max_512gb_silver_l59358_14.png',
    mainCamera:
      'Siêu rộng: khẩu độ ƒ / 2.4. Nắp ống kính tinh thể sapphire, 100% Focus Pixels (Rộng), Hệ thống camera 12MP chuyên nghiệp',
    pin: '2815 mhA',
  },
  {
    id: 'MH376',
    name: 'iphone 12',
    url1:
      'https://images.anandtech.com/doci/16167/GEO-iPhone12-color-lineup-6up_575px.png',
    capacity: '256gb',
    price: 25890000,
    amount: 5,
    screenSize: '6.1", Lớp phủ oleophobic chống bám vân tay',
    screenResolution: '2532 x 1170 pixel 460 ppi',
    cpu: 'A14 Bionci',
    ram: '4 GB',
    url2: 'https://techalpha.vn/wp-content/uploads/2020/09/iphone_12_mini.png',
    url3:
      'https://product.hstatic.net/1000403181/product/iphone-12-mini-white-select-2020_b27338a15d554248aafadce971d8aa7d.png',
    url4:
      'https://www.sunrise.ch/var/commerce/products/web/Default/en/apple_iphone_12_miniparent/picture.1602749845357.transform/original/20919_20924_20929iphone12miniblueproductimage1000x1000png.png',
    url5:
      'https://bizweb.dktcdn.net/100/116/615/products/12mini-b6612cdd-bb3f-4bec-a95d-22428f3649a4.png?v=1602751351000',
    mainCamera: 'Siêu rộng: Khẩu độ f2.4, hệ thống camera kép 12MP',
    pin: '2815 mhA',
  },
  {
    id: 'MH377',
    name: 'iphone 12',
    url1:
      'https://images.anandtech.com/doci/16167/GEO-iPhone12-color-lineup-6up_575px.png',
    capacity: '128gb',
    price: 23390000,
    amount: 5,
    screenSize: '6.1", Lớp phủ oleophobic chống bám vân tay',
    screenResolution: '2532 x 1170 pixel 460 ppi',
    cpu: 'A14 Bionci',
    ram: '4 GB',
    url2: 'https://techalpha.vn/wp-content/uploads/2020/09/iphone_12_mini.png',
    url3:
      'https://product.hstatic.net/1000403181/product/iphone-12-mini-white-select-2020_b27338a15d554248aafadce971d8aa7d.png',
    url4:
      'https://www.sunrise.ch/var/commerce/products/web/Default/en/apple_iphone_12_miniparent/picture.1602749845357.transform/original/20919_20924_20929iphone12miniblueproductimage1000x1000png.png',
    url5:
      'https://bizweb.dktcdn.net/100/116/615/products/12mini-b6612cdd-bb3f-4bec-a95d-22428f3649a4.png?v=1602751351000',
    mainCamera: 'Siêu rộng: Khẩu độ f2.4, hệ thống camera kép 12MP',
    pin: '2815 mhA',
  },
  {
    id: 'MH378',
    name: 'iphone 12',
    url1:
      'https://images.anandtech.com/doci/16167/GEO-iPhone12-color-lineup-6up_575px.png',
    capacity: '64gb',
    price: 21890000,
    amount: 5,
    screenSize: '6.1", Lớp phủ oleophobic chống bám vân tay',
    screenResolution: '2532 x 1170 pixel 460 ppi',
    cpu: 'A14 Bionci',
    ram: '4 GB',
    url2: 'https://techalpha.vn/wp-content/uploads/2020/09/iphone_12_mini.png',
    url3:
      'https://product.hstatic.net/1000403181/product/iphone-12-mini-white-select-2020_b27338a15d554248aafadce971d8aa7d.png',
    url4:
      'https://www.sunrise.ch/var/commerce/products/web/Default/en/apple_iphone_12_miniparent/picture.1602749845357.transform/original/20919_20924_20929iphone12miniblueproductimage1000x1000png.png',
    url5:
      'https://bizweb.dktcdn.net/100/116/615/products/12mini-b6612cdd-bb3f-4bec-a95d-22428f3649a4.png?v=1602751351000',
    mainCamera: 'Siêu rộng: Khẩu độ f2.4, hệ thống camera kép 12MP',
    pin: '2815 mhA',
  },
  {
    id: 'MH379',
    name: 'iphone 12 mini',
    url1:
      'https://images.anandtech.com/doci/16167/GEO-iPhone12-color-lineup-6up_575px.png',
    capacity: '256gb',
    price: 23150000,
    amount: 5,
    screenSize: '5.4", Super Retina XDR',
    screenResolution: '2340 x 1080 pixel 476 ppi',
    cpu: 'A14 Bionci',
    ram: '4 GB',
    url2: 'https://techalpha.vn/wp-content/uploads/2020/09/iphone_12_mini.png',
    url3:
      'https://product.hstatic.net/1000403181/product/iphone-12-mini-white-select-2020_b27338a15d554248aafadce971d8aa7d.png',
    url4:
      'https://www.sunrise.ch/var/commerce/products/web/Default/en/apple_iphone_12_miniparent/picture.1602749845357.transform/original/20919_20924_20929iphone12miniblueproductimage1000x1000png.png',
    url5:
      'https://bizweb.dktcdn.net/100/116/615/products/12mini-b6612cdd-bb3f-4bec-a95d-22428f3649a4.png?v=1602751351000',
    mainCamera: 'Siêu rộng: Khẩu độ f2.4, hệ thống camera kép 12MP',
    pin: '2227 mhA',
  },
  {
    id: 'MH380',
    name: 'iphone 12 mini',
    url1:
      'https://images.anandtech.com/doci/16167/GEO-iPhone12-color-lineup-6up_575px.png',
    capacity: '128gb',
    price: 20490000,
    amount: 5,
    screenSize: '5.4", Super Retina XDR',
    screenResolution: '2340 x 1080 pixel 476 ppi',
    cpu: 'A14 Bionci',
    ram: '4 GB',
    url2: 'https://techalpha.vn/wp-content/uploads/2020/09/iphone_12_mini.png',
    url3:
      'https://product.hstatic.net/1000403181/product/iphone-12-mini-white-select-2020_b27338a15d554248aafadce971d8aa7d.png',
    url4:
      'https://www.sunrise.ch/var/commerce/products/web/Default/en/apple_iphone_12_miniparent/picture.1602749845357.transform/original/20919_20924_20929iphone12miniblueproductimage1000x1000png.png',
    url5:
      'https://bizweb.dktcdn.net/100/116/615/products/12mini-b6612cdd-bb3f-4bec-a95d-22428f3649a4.png?v=1602751351000',
    mainCamera: 'Siêu rộng: Khẩu độ f2.4, hệ thống camera kép 12MP',
    pin: '2227 mhA',
  },
  {
    id: 'MH381',
    name: 'iphone 12 mini',
    url1:
      'https://images.anandtech.com/doci/16167/GEO-iPhone12-color-lineup-6up_575px.png',
    capacity: '64gb',
    price: 18990000,
    amount: 5,
    screenSize: '5.4", Super Retina XDR',
    screenResolution: '2340 x 1080 pixel 476 ppi',
    cpu: 'A14 Bionci',
    ram: '4 GB',
    url2: 'https://techalpha.vn/wp-content/uploads/2020/09/iphone_12_mini.png',
    url3:
      'https://product.hstatic.net/1000403181/product/iphone-12-mini-white-select-2020_b27338a15d554248aafadce971d8aa7d.png',
    url4:
      'https://www.sunrise.ch/var/commerce/products/web/Default/en/apple_iphone_12_miniparent/picture.1602749845357.transform/original/20919_20924_20929iphone12miniblueproductimage1000x1000png.png',
    url5:
      'https://bizweb.dktcdn.net/100/116/615/products/12mini-b6612cdd-bb3f-4bec-a95d-22428f3649a4.png?v=1602751351000',
    mainCamera: 'Siêu rộng: Khẩu độ f2.4, hệ thống camera kép 12MP',
    pin: '2227 mhA',
  },
];

export const CartData = [];
