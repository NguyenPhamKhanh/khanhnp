import React from 'react';
import {View, Text, Button, Alert} from 'react-native';
import {TouchableOpacity, TextInput} from 'react-native-gesture-handler';
import {useDispatch} from 'react-redux';
import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook,
} from 'react-redux';
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;
import {RootState} from '../../redux/reducer';

import styles from './styles';
import {updateProductAmount} from '../../redux/actions/product.actions';
import formatNumber from '../../format/formatNumber';

const CheckOutScreen = ({route, navigation}) => {
  let receiveData = route.params;
  let totalMoney = receiveData.totalMoney;
  const dispatch = useDispatch();
  const cartData = useSelector((state: RootState) => state.cartReducer.cart);
  const productData = useSelector(
    (state: RootState) => state.productReducer.product,
  );

  const onPressConfirmOrder = () => {
    dispatch(updateProductAmount(productData, cartData));
    Alert.alert(
      'Thanh toán thành công. Quý khách vui lòng đợi 1-2 ngày để nhận sản phẩm',
    );
  };

  const continueListItem = () => {
    navigation.navigate('ListScreen');
  };

  return (
    <View>
      <View>
        <Button title="mua hang tiep" onPress={continueListItem} />
      </View>
      <View style={styles.view21}>
        <View style={styles.view211}>
          <Text style={styles.text211}>Đơn vị vận chuyển:</Text>
          <Text style={styles.text211}>NinJa Van</Text>
        </View>

        <View style={styles.view212}>
          <Text style={styles.text211}>Tin nhắn:</Text>
          <TextInput
            style={styles.text211}
            placeholder=".......fill mess here"
          />
        </View>

        <View style={styles.view213}>
          <Text style={styles.text211}>Shop Voucher:</Text>
          <TextInput style={styles.text211} placeholder="fill Voucher here" />
        </View>

        <View style={styles.view214}>
          <Text style={styles.text214}>Phương thức thanh toán:</Text>
          <TouchableOpacity style={styles.text214}>
            <Text>Chọn phương thức thanh toán</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.view22}>
        <Text style={styles.text}>
          Tổng thanh toán: {formatNumber(totalMoney)}
        </Text>
        <TouchableOpacity
          style={styles.touchDatHang}
          onPress={onPressConfirmOrder}>
          <Text style={styles.text}>Đặt hàng</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CheckOutScreen;
