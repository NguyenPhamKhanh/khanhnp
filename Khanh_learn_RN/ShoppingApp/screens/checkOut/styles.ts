import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  view21: {
    marginTop: 30,
    marginBottom: 20,
    height: 500,
    justifyContent: 'flex-end',
  },
  view211: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  text211: {
    marginTop: 20,
    fontSize: 15,
    margin: 10,
  },

  view212: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  text212: {
    marginTop: 20,
    fontSize: 15,
    margin: 10,
  },

  view213: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  text213: {
    marginTop: 20,
    fontSize: 15,
    margin: 10,
  },

  view214: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  text214: {
    marginTop: 20,
    fontSize: 15,
    margin: 10,
  },

  text21: {
    margin: 10,
    fontSize: 15,
  },

  view22: {
    flexDirection: 'row',
    marginLeft: 20,
    marginTop: 30,
    marginEnd: 30,
  },
  touchDatHang: {
    width: 80,
    height: 25,
    marginLeft: 20,
    backgroundColor: 'red',
  },
  text: {
    width: 270,
    fontSize: 18,
    fontWeight: 'bold',
  },
});
export default styles;
