import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  view: {
    flex: 1,
    padding: 15,
    margin: 8,
    width: 190,
  },

  container: {
    borderRadius: 40,
  },

  textItem: {
    fontSize: 12,
    textAlign: 'center',
    marginBottom: 5,
  },

  money: {
    fontSize: 13,
    textAlign: 'center',
    marginBottom: 5,
    fontWeight: 'bold',
    color: 'red',
  },

  touch: {
    flexDirection: 'row',
    marginTop: 5,
    marginLeft: 340,
    width: 100,
  },
  textTouch: {
    fontSize: 20,
  },
  image: {
    justifyContent: 'space-between',
    marginLeft: 30,
    width: 150,
    height: 150,
  },
  cart: {
    width: 30,
    height: 30,
    marginBottom: 20,
  },
});

export default styles;
