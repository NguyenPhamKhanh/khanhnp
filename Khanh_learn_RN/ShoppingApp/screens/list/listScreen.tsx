/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react';
import {
  Text,
  FlatList,
  TouchableOpacity,
  View,
  ImageBackground,
} from 'react-native';
import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook,
} from 'react-redux';
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;
import {RootState} from '../../redux/reducer';
import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation';

import {products} from '../../mockup/products';
import styles from './styles';
import formatNumber from '../../format/formatNumber';

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

const listScreen = (props: Props) => {
  const getCartLength = useSelector(
    (state: RootState) => state.cartReducer.cart.length,
  );

  const onPressCart = () => {
    props.navigation.navigate('CartScreen');
  };

  const renderItem = (itemData) => {
    const clickItem = () => {
      props.navigation.navigate('ListDetailScreen', {
        id: itemData.item.id,
      });
    };
    return (
      <TouchableOpacity onPress={clickItem} style={styles.container}>
        <ImageBackground
          source={{
            uri: itemData.item.url1,
          }}
          style={styles.image}
        />
        <TouchableOpacity style={styles.view} onPress={clickItem}>
          <Text style={styles.textItem}>
            {itemData.item.name} - {itemData.item.capacity}
          </Text>
          <Text style={styles.money}>
            {formatNumber(itemData.item.price)} VND
          </Text>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  return (
    <View>
      <TouchableOpacity style={styles.touch} onPress={onPressCart}>
        <ImageBackground
          source={{
            uri:
              'https://pngimg.com/uploads/shopping_cart/shopping_cart_PNG42.png',
          }}
          style={styles.cart}
        />
        <Text style={styles.textTouch}>{getCartLength}</Text>
      </TouchableOpacity>
      <FlatList
        data={products}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        numColumns={2}
      />
    </View>
  );
};

export default listScreen;
