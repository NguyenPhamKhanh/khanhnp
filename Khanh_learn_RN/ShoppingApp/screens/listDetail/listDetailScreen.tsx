/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react';
import {View, Text, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch} from 'react-redux';
import Swiper from 'react-native-swiper';
import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook,
} from 'react-redux';
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;
import {RootState} from '../../redux/reducer';

import {IncrementCart} from '../../redux/actions/cart.actions';
import {products} from '../../mockup/products';
import styles from './styles';
import formatNumber from '../../format/formatNumber';

const listDetailScreen = ({route, navigation}) => {
  const dispatch = useDispatch();
  const productData = useSelector(
    (state: RootState) => state.productReducer.product,
  );
  const cartData = useSelector((state: RootState) => state.cartReducer.cart);

  const receiveData = route.params;
  let product = products.find((data) => data.id === receiveData.id);

  const addToCart = () => {
    navigation.navigate('CartScreen');
    dispatch(IncrementCart(product, productData, cartData));
  };

  const arr = [];
  arr.push(product.url1);
  arr.push(product.url2);
  arr.push(product.url3);
  arr.push(product.url4);
  arr.push(product.url5);

  return (
    <View style={styles.view}>
      <Swiper style={styles.wrapper} showsButtons={false}>
        {arr.map((image, index) => (
          <Image
            key={index}
            source={{uri: image}}
            style={styles.image}
            width={300}
          />
        ))}
      </Swiper>
      <Text style={styles.text}>Tên sản phẩm: {product.name}</Text>
      <Text style={styles.text}>Kích thước màn hình: {product.screenSize}</Text>
      <Text style={styles.text}>
        Độ phân giải màn hình: {product.screenResolution}
      </Text>
      <Text style={styles.text}>Chip xử lý: {product.cpu}</Text>
      <Text style={styles.text}>Ram: {product.ram}</Text>
      {/* <Text style={styles.text}>Máy ảnh chính: {product.mainCamera}</Text> */}
      <Text style={styles.text}>Bộ nhớ trong: {product.capacity}</Text>
      <Text style={styles.text}>Dung lượng pin: {product.pin}</Text>
      <Text style={styles.text}>Giá: {formatNumber(product.price)}</Text>

      <View>
        <TouchableOpacity style={styles.touch} onPress={addToCart}>
          <Text style={styles.textTouch}>Thêm vào giỏ hàng</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default listDetailScreen;
