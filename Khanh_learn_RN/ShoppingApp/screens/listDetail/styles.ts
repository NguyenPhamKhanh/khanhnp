import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  wrapper: {},
  view: {
    flex: 1,
    padding: 10,
  },
  image: {
    marginLeft: 50,
    width: 250,
    height: 250,
  },
  text: {
    padding: 10,
    fontSize: 20,
    fontWeight: 'bold',
  },
  touch: {
    backgroundColor: 'green',
    borderRadius: 100,
    width: 350,
    height: 80,
    marginTop: 30,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20,
  },
  textTouch: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default styles;
