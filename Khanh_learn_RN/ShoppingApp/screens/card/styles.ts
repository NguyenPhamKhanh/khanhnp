import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  view21: {
    marginLeft: 10,
    flexDirection: 'row',
    // backgroundColor: 'red',
  },
  text21: {
    fontSize: 20,
    marginLeft: 10,
    color: 'red',
    fontWeight: 'bold',
    // backgroundColor: 'green',
  },

  view: {
    flexDirection: 'row',
    padding: 10,
    marginTop: 10,
  },
  text: {
    width: 200,
    fontSize: 20,
    textAlign: 'center',
  },
  price: {
    marginLeft: 20,
    fontSize: 20,
    // backgroundColor: 'red',
  },
  total: {
    marginTop: 30,
    height: 40,
    fontSize: 30,
    textAlign: 'center',
    justifyContent: 'center',
  },
  viewBTN: {
    flexDirection: 'row',
  },
  confirm: {
    marginTop: 30,
    height: 40,
    width: 110,
    marginLeft: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  textConfirm: {
    textAlign: 'center',
    fontSize: 20,
  },
  continue: {
    marginTop: 30,
    height: 40,
    width: 180,
    marginLeft: 30,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
});
export default styles;
