/* eslint-disable no-alert */
import React from 'react';
import {Text, View} from 'react-native';
import {useDispatch} from 'react-redux';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook,
} from 'react-redux';
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;

import styles from './styles';
import {products} from '../../mockup/products';
import {IncrementCart, DecrementCart} from '../../redux/actions/cart.actions';
import formatNumber from '../../format/formatNumber';
import {RootState} from '../../redux/reducer';
import {
  NavigationScreenProp,
  NavigationState,
  NavigationParams,
} from 'react-navigation';

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

const CartScreen = (props: Props) => {
  const productData = useSelector(
    (state: RootState) => state.productReducer.product,
  );
  const cartData = useSelector((state: RootState) => state.cartReducer.cart);

  const dispatch = useDispatch();

  const totalMoney = (obj) => {
    let total = 0;
    for (let i = 0; i < obj.length; i++) {
      total += obj[i].price * obj[i].amount;
    }
    return total;
  };

  let money = totalMoney(cartData);

  const onClickThanhToan = () => {
    if (money === 0) {
      alert('Giỏ hàng đang trống');
      return;
    }
    props.navigation.navigate('CheckOutScreen', {
      totalMoney: money,
    });
  };

  const onClickComeBack = () => {
    props.navigation.navigate('ListScreen');
  };

  const renderItem = (itemData) => {
    let product = products.find((data) => data.id === itemData.item.id);
    let id = itemData.item.id;

    const onClickIncrement = () => {
      dispatch(IncrementCart(product, productData, cartData));
    };
    const onDeleteItemCart = () => {
      dispatch(DecrementCart(id, productData, cartData));
    };

    return (
      <View>
        <TouchableOpacity style={styles.view}>
          <Text style={styles.text}>
            {itemData.item.name} - {itemData.item.capacity}
          </Text>
          <View style={styles.view21}>
            <TouchableOpacity onPress={onDeleteItemCart}>
              <Text style={styles.text21}>-</Text>
            </TouchableOpacity>
            <Text style={styles.text21}>{itemData.item.amount}</Text>
            <TouchableOpacity onPress={onClickIncrement}>
              <Text style={styles.text21}>+</Text>
            </TouchableOpacity>
          </View>
          <Text style={styles.price}>
            {formatNumber(itemData.item.price * itemData.item.amount)}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View>
      <FlatList
        data={cartData}
        renderItem={renderItem}
        keyExtractor={(item: object) => item.id.toString()}
      />
      <Text style={styles.total}>
        Thành tiền: {formatNumber(totalMoney(cartData))}
      </Text>
      <View style={styles.viewBTN}>
        <TouchableOpacity style={styles.continue} onPress={onClickComeBack}>
          <Text style={styles.textConfirm}>Xem mặt hàng khác</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.confirm} onPress={onClickThanhToan}>
          <Text style={styles.textConfirm}>Thanh toán</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CartScreen;
