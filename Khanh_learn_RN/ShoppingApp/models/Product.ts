export interface IProduct {
  id: string;
  name: string;
  capacity: string;
  price: number;
  amount: number;
  screenSize: string;
  screenResolution: string;
  cpu: string;
  ram?: string;
  mainCamera?: string;
  pin?: string;
  url1?: string;
  url2?: string;
  url3?: string;
  url4?: string;
  url5?: string;
}
