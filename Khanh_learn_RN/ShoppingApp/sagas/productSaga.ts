/* eslint-disable @typescript-eslint/no-unused-vars */
import {takeLatest, call, put} from 'redux-saga/effects';

import {updateProductAmountSucces} from '../redux/actions/product.actions';
import * as types from '../redux/constants';
import {emptyCart} from '../redux/actions/cart.actions';

export function* updateProductAmount(data) {
  let element1 = 0;
  let element2 = 0;
  for (let i = 0; i < data.cartData.length; i++) {
    element1 = data.cartData[i].id;
    for (let j = 0; j < data.productData.length; j++) {
      element2 = data.productData[j].id;
      if (element1 === element2) {
        data.productData = data.productData
          .filter((state) => state.id !== element1)
          .concat([
            {
              id: element1,
              name: data.productData[j].name,
              capacity: data.productData[j].capacity,
              price: data.productData[j].price,
              amount: data.productData[j].amount - data.cartData[i].amount,
              screenSize: data.productData[j].screenSize,
              screenResolution: data.productData[j].screenResolution,
              cpu: data.productData[j].cpu,
              ram: data.productData[j].ram,
              mainCamera: data.productData[j].mainCamera,
              pin: data.productData[j].pin,
            },
          ]);
        data.productData = data.productData.sort(function (a, b) {
          return b.price - a.price;
        });
      }
    }
  }
  yield put(updateProductAmountSucces(data.productData));
  yield put(emptyCart());
}

export function* amountInProduct() {
  yield takeLatest(types.UPDATEPRODUCTAMOUNT, updateProductAmount);
}
