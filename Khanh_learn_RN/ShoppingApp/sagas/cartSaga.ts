/* eslint-disable no-alert */
/* eslint-disable @typescript-eslint/no-unused-vars */
import {takeLatest, call, put} from 'redux-saga/effects';

import {
  DecrementCartSucces,
  IncrementCartSucces,
} from '../redux/actions/cart.actions';
import * as types from '../redux/constants';

export function* incrememtCartSaga(data) {
  let obj = '';
  obj = data.cartData.find((state) => state.id === data.data.id);

  //if !CartData.includes(item) => add item to cart
  if (obj === '' || obj === undefined) {
    let amountInProduct = data.productData.filter(
      (state) => state.id === data.data.id,
    );
    data.cartData = data.cartData.concat([
      {
        id: data.data.id,
        name: data.data.name,
        capacity: data.data.capacity,
        price: data.data.price,
        amount: amountInProduct[0].amount === 0 ? 0 : 1,
        screenSize: data.data.screenSize,
        screenResolution: data.data.screenResolution,
        cpu: data.data.cpu,
        ram: data.data.ram,
        mainCamera: data.data.mainCamera,
        pin: data.data.pin,
      },
    ]);
  }
  //if CartData.includes(item) => count amount += 1
  else {
    let info = data.cartData.filter((state) => state.id === data.data.id);
    let amountInProduct = data.productData.filter(
      (state) => state.id === data.data.id,
    );
    if (info[0].amount >= amountInProduct[0].amount) {
      alert('Không đủ số lượng tồn kho');
      return;
    }

    data.cartData = data.cartData
      .filter((state) => state.id !== data.data.id)
      .concat([
        {
          id: data.data.id,
          name: data.data.name,
          capacity: data.data.capacity,
          price: data.data.price,
          amount: (info[0].amount += 1),
          screenSize: data.data.screenSize,
          screenResolution: data.data.screenResolution,
          cpu: data.data.cpu,
          ram: data.data.ram,
          mainCamera: data.data.mainCamera,
          pin: data.data.pin,
        },
      ]);
  }
  //sort item in Cart
  data.cartData = data.cartData.sort(function (a, b) {
    return b.price - a.price;
  });
  yield put(IncrementCartSucces(data.cartData));
}

export function* decrememtCartSaga(data) {
  let obj = data.cartData.filter((da) => da.id === data.id);
  //if amount === 1 => delete item
  if (obj[0].amount === 1 || obj[0].amount === 0) {
    data.cartData = data.cartData.filter((da) => da.id !== data.id);
  }
  //if amount > 1 => count amount -=1
  else {
    data.cartData = data.cartData
      .filter((state) => state.id !== data.id)
      .concat([
        {
          id: obj[0].id,
          name: obj[0].name,
          capacity: obj[0].capacity,
          price: obj[0].price,
          amount: (obj[0].amount -= 1),
          screenSize: obj[0].screenSize,
          screenResolution: obj[0].screenResolution,
          cpu: obj[0].cpu,
          ram: obj[0].ram,
          mainCamera: obj[0].mainCamera,
          pin: obj[0].pin,
        },
      ]);
  }
  //sort item in Cart
  data.cartData = data.cartData.sort(function (a, b) {
    return b.price - a.price;
  });
  yield put(DecrementCartSucces(data.cartData));
}

export function* addCartSaga() {
  yield takeLatest(types.INCREMENTCART, incrememtCartSaga);
}

export function* deleteCartSaga() {
  yield takeLatest(types.DECREMENTCART, decrememtCartSaga);
}
