import {all} from 'redux-saga/effects';

import {amountInProduct} from './productSaga';
import {addCartSaga, deleteCartSaga} from './cartSaga';

export default function* rootSaga() {
  yield all([amountInProduct(), deleteCartSaga(), addCartSaga()]);
}
