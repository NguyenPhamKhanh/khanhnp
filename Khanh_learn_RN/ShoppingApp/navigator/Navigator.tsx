import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import listScreen from '../screens/list/listScreen';
import listDetailScreen from '../screens/listDetail/listDetailScreen';
import CartScreen from '../screens/card/cartScreen';
import CheckOutScreen from '../screens/checkOut/checkOutScreen';

const Navigator = () => {
  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="ListScreen"
          component={listScreen}
          options={{headerTitle: 'Iphone 12 series'}}
        />
        <Stack.Screen
          name="ListDetailScreen"
          component={listDetailScreen}
          options={{headerTitle: 'Chi tiết sản phẩm'}}
        />
        <Stack.Screen
          name="CartScreen"
          component={CartScreen}
          options={{headerTitle: 'Giỏ hàng'}}
        />
        <Stack.Screen
          name="CheckOutScreen"
          component={CheckOutScreen}
          options={{headerTitle: 'Thanh toán'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
